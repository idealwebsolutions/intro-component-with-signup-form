/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {
      fontFamily: {
        'poppins': ['Poppins', 'sans-serif']
      },
      backgroundImage: {
        'mobile-intro': 'url("./images/bg-intro-mobile.png")',
        'desktop-intro': 'url("./images/bg-intro-desktop.png")'
      },
      colors: {
        'custom-red': 'hsl(0, 100%, 74%)',
        'custom-green': 'hsl(154, 59%, 51%)',
        'custom-blue': 'hsl(248, 32%, 49%)',
        'dark-blue': 'hsl(249, 10%, 26%)',
        'grayish-blue': 'hsl(246, 25%, 77%)'
      },
      fontSize: {
        xxs: '0.675rem'
      },
      margin: {
        '15': '3.75rem'
      },
      boxShadow: {
        'custom': '0 15px 6px -6px rgba(0,0,0,0.3)',
        'custom-form': '0 9px 0px rgba(0,0,0,0.3)'
      }
    },
  },
  plugins: [],
}
