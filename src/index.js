/* 
    This solution aims to use an alternative approach by minimal js to take advantage of the built-in html5 validation while relying mostly on css to display error messages.

    Problem: Since peer combinators cannot change styles on themselves (only siblings), javascript is required to achieve a similar result. 
    Workaround: The idea is that when validity is reported, each form element with invalid input will "mark" the sibling peer as checked.
    Using the checked state, sibling peer elements can be styled as if though the main peer (input) was marked invalid.
*/
document.addEventListener('DOMContentLoaded', () => {
  const form = document.getElementById('signup-form');
  const firstName = document.getElementById('first-name');
  const lastName = document.getElementById('last-name');
  const email = document.getElementById('email');
  const password = document.getElementById('password');

  const createInvalidListener = (node) => {
    return (e) => {
      e.preventDefault();
      node.setCustomValidity('');
      node.parentNode.querySelector('.peer').checked = true;
      return false;
    };
  };

  const createInputListener = (node) => {
    return (e) => {
      if (node.checkValidity()) {
        node.parentNode.querySelector('.peer').checked = false;
      }
      node.parentNode.querySelector('.peer').setAttribute('disabled', 'disabled');
    }
  };

  // These invalid listeners are invoked when checkValidity is called
  const fnInvalidListener = createInvalidListener(firstName);
  const lnInvalidListener = createInvalidListener(lastName);
  const emInvalidListener = createInvalidListener(email);
  const pwInvalidListener = createInvalidListener(password);
 
  // Follow input changes and remove errors in realtime
  firstName.addEventListener('input', createInputListener(firstName));
  lastName.addEventListener('input', createInputListener(lastName));
  email.addEventListener('input', createInputListener(email));
  password.addEventListener('input', createInputListener(password));

  // Check for initial errors once the form is submitted by adding their constraints dynamically
  form.addEventListener('submit', (e) => {
    e.preventDefault(); // Do not submit
    firstName.setAttribute('minlength', '1');
    firstName.setAttribute('required', 'required');
    firstName.removeEventListener('invalid', fnInvalidListener);
    firstName.addEventListener('invalid', fnInvalidListener);
    lastName.setAttribute('minlength', '1');
    lastName.setAttribute('required', 'required');
    lastName.removeEventListener('invalid', lnInvalidListener);
    lastName.addEventListener('invalid', lnInvalidListener);
    email.setAttribute('type', 'email'); // Change type from text to email to prevent pre-validation before form submission
    email.setAttribute('minlength', '3');
    email.setAttribute('pattern', '[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9]{2,61}(?:[a-zA-Z0-9-.][a-zA-Z0-9]{2,61})*');
    email.setAttribute('required', 'required');
    email.removeEventListener('invalid', emInvalidListener);
    email.addEventListener('invalid', emInvalidListener);
    password.setAttribute('minlength', '1');
    password.setAttribute('required', 'required');
    password.removeEventListener('invalid', pwInvalidListener);
    password.addEventListener('invalid', pwInvalidListener);
    if (form.reportValidity()) {
        form.reset(); // Emulate submit and throw away data
    }
  });
});